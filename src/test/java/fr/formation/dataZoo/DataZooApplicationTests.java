package fr.formation.dataZoo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fr.formation.dataZoo.bll.AnimalManager;
import fr.formation.dataZoo.bll.ZooException;
import fr.formation.dataZoo.bo.Animal;


@RunWith(SpringRunner.class)
@SpringBootTest
public class DataZooApplicationTests {
	
	@Autowired
	AnimalManager manager;

	@Test
	public void contextLoads() {
		
		try {
			manager.addAnimal(new Animal("Roger", "Lion", "Mâle", "Fauve"));
		} catch(ZooException e) {
			System.out.println(e.getMessage());
		}
		
		try {
			manager.addAnimal(new Animal("Vivi","Goeland", "Femelle", "oiseau"));
		} catch (ZooException e) {
			System.out.println(e.getMessage());
		}
		
		try {
			manager.addAnimal(new Animal("Roger","chat", "Mâle", "Félin"));
		} catch (ZooException e) {
			System.out.println(e.getMessage());
		}
		try {
			manager.addAnimal(new Animal("Loulou","carpe", "Mâle", "poisson"));
		} catch (ZooException e) {
			System.out.println(e.getMessage());
		}
		
		
		System.out.println(manager.getAllAnimal());
		System.out.println(manager.getAllMale());
	}

}

