package fr.formation.dataZoo.dal;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.formation.dataZoo.bo.Animal;

public interface AnimalRepository extends CrudRepository <Animal, Integer>{

	
	List<Animal> findBySexe(String sexe);

	List<Animal> findByRaceAndSexe(String race, String sexe);

	List<Animal> findByNom(String nom);

	

}
