package fr.formation.dataZoo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataZooApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataZooApplication.class, args);
	}

}

