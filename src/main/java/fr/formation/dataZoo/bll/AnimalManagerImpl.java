package fr.formation.dataZoo.bll;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.formation.dataZoo.bo.Animal;
import fr.formation.dataZoo.dal.AnimalRepository;

@Service
public class AnimalManagerImpl implements AnimalManager{
	
	@Autowired
	AnimalRepository dao;

	@Override
	public void addAnimal(Animal animal) throws ZooException {
		if (valid(animal)){
			dao.save(animal);
		}
		
	}

	private boolean valid(Animal animal) throws ZooException {
		if("Fauve".equalsIgnoreCase(animal.getCategorie())) {
			throw new ZooException ("Pas de fauve !!");
		}
		
		if("mâle".equalsIgnoreCase(animal.getSexe()) && dao.findByRaceAndSexe(animal.getRace(), animal.getSexe()).size()>0) {
			
		}
		
		if(dao.findByNom(animal.getNom()).size()>0) {
			
		}
		
		return true;
	}

	@Override
	public List<Animal> getAllAnimal() {
		return (List<Animal>) dao.findAll();
	}

	@Override
	public List<Animal> getAllMale() {
		return (List<Animal>) dao.findBySexe("mâle");
	}

}
