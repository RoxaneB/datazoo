package fr.formation.dataZoo.bll;

public class ZooException extends Exception{
	
	public ZooException(String message) {
		super(message);
	}

}
