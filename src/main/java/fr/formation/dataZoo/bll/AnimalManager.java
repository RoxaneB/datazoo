package fr.formation.dataZoo.bll;

import java.util.List;

import fr.formation.dataZoo.bo.Animal;


public interface AnimalManager {
	
	// methodes get dans manager et find dans repository
	public void addAnimal(Animal animal) throws ZooException;

	public List<Animal> getAllAnimal();
	
	public List<Animal> getAllMale();

}
